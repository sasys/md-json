//! # A REGEX used to find sections in the markdown. 
/// A section looks like:
/// ## id title  
///
/// This regex is separated here because it causes the bitbucket code render to 
/// make a mess.   
 
use lazy_static::lazy_static;
use regex::Regex;

lazy_static! {
    pub static ref HASHTAG_REGEX : Regex = Regex::new(
            r#"#+\s*[\.|0-9]*\s+[.0-9a-zA-Z_!\s,|\\:\-`()\\"\\<\\>\\[\\]\*/+]+[\\.]*"#).unwrap();
}