//! # A Parser For The Markdown Version Of The Model
 
/// Given a markdown model, extracts the textual hierarchy of ids, titles and body texts 
/// into a composite structure.
/// 
/// # Input A markdown string representing the complete model
/// 
/// # Output A composite instances containing the ids, titles and body descriptions.
/// 


use std::fs::File;
use std::io::{Read, Error};

use patterns::{ArborealBranchType, Vis2, Visitable};
use super::regex_constant::{HASHTAG_REGEX};


#[derive(Debug)]
pub struct BlockType {
    level_index: u8,// 0 =>0, 1 => 1, 1.1 =>2, 1.2.1 =>3
    index: String,  //eg 1.2.1
    title: String,
    text:  String,
    parent: String
}

impl<'a> PartialEq for BlockType {
    fn eq(&self, other: &Self) -> bool {
        self.level_index == other.level_index &&
        self.index == other.index &&
        self.title == other.title &&
        self.parent == other.parent &&
        self.text == other.text
    }
}

impl<'a> Eq for BlockType {}


impl<'a> std::fmt::Display for BlockType {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        write!(f, r#"{{"id" : "{}", "title" :  "{}", "description" : "{}", "data-in":[], "data-out":[],"children":"#, self.index, self.title, self.text)
    }
}


impl<'a> Clone for BlockType {
    fn clone(&self) -> BlockType {
        BlockType{level_index: self.level_index,
                  index: self.index.clone(),
                  title: self.title.clone(),
                  text: self.text.clone(),
                  parent: self.parent.clone()}
    }
}


fn parse_id(id: &str) -> Vec<i32> {
    if id == "0" {
        vec![0]
    }
    else if id.len() == 1 {
        let i: i32 = id.parse().unwrap();
        if i >= 1 {
            vec![i]
        }
        else {
            vec![0i32]
        }
    }
    else {
        let coordinates: Vec<&str> = id.split('.').collect();
        let s_coordinates: Vec<i32> = coordinates.into_iter().map(|c| c.to_string().parse().unwrap()).collect();
        s_coordinates
    }
}

//given two ids are they the same
pub fn same_id(id_1: &str, id_2: &str) -> bool {
    id_1.eq_ignore_ascii_case(id_2)
}

//given two ids, is the first one higher than the second
pub fn higher_than(id_1: &str, id_2: &str) -> bool {
    id_1.chars().count() > id_2.chars().count() 
}

//given two ids, is the first one higher than the second
pub fn find_distance(id_1: &str, id_2: &str) -> i32 {
    id_1.split('.').count() as i32 - id_2.split('.').count() as i32
}

//Is the first id a child of the second id
pub fn child_of(id_1: &str, id_2: &str) -> bool {
    if id_1.chars().count() as i32 - id_2.chars().count() as i32 >= 1 {
        true
    } else {
        false
    }
}

//if either are the context return false
pub fn siblings(id_1: &str, id_2: &str) -> bool {
    if id_1 == "0" || id_2 == "0" {
        false
    }
    else {
        let same_level = same_level(id_1, id_2);
        let id_1_parent = level_parent(id_1).0;
        let id_2_parent = level_parent(id_2).0;
        same_level && id_1_parent == id_2_parent
    }
}


//given two ids, is the first one deeper than the second
pub fn lower_than(id_1: &str, id_2: &str) -> bool {
    id_1.chars().count() < id_2.chars().count()
}

//given two ids are they the same level
pub fn same_level(id_1: &str, id_2: &str) -> bool {
    id_1.chars().count() == id_2.chars().count()
}

//given an index like 1.1.1, return the parent and the level like 
// 0 => (0,0)
// 1 => (0, 1)
// 1.1 => (1, 2)
// 1.3.2.4 => (1.3.2, 4)
fn level_parent(id: &str) -> (String, u8) {
    let default_parent: String = "0".to_string();
    if id == "0" {
        (default_parent, 0)
    }
    else if id.chars().count() == 1 {

        (default_parent, 1)
    }
    else {
        let len = id.len();
        let coordinates: Vec<&str> = id.split('.').collect();
        let level = coordinates.len() as u8;
        let default_parent = &id[0..len-2];
        (default_parent.to_owned(), level)
    }
}

//Given a level like 1.1 find the children (like 1.1.1, 1.1.2 etc)
//The context which is "0" is taken as a special case.
pub fn level_immediate_children(parent_id: &str, blocks: &[BlockType]) -> Vec<BlockType> {
    let parent_id_len = parent_id.chars().count();
    let target_len; 
    let c1: Vec<BlockType>;
    if parent_id == "0" {
        target_len = 1;
        c1 = blocks.iter().filter(|b|b.index.len() == target_len && b.index != "0").cloned().collect();
    }
    else {
        target_len = parent_id_len + 2;
        c1 = blocks.iter().filter(|b| b.index.chars().count() == target_len && b.index.chars().take(parent_id_len).collect::<String>() == parent_id).cloned().collect();
    } 
    let children: Vec<BlockType> = c1.into_iter().collect();
    children
}


fn parse_section(section: String) -> BlockType {

    let split = section.split('\n').collect::<Vec<&str>>();
    let title_split = split[0].split(' ').collect::<Vec<&str>>();

    if title_split.len() >= 3 {
        let (head, _) = title_split.split_at(2);
        let i =  head[1].to_owned();
        let (p,l) = level_parent(&i);
        let b =   BlockType{level_index: l,//head[0].chars().count() as u8,
                                       index: i,
                                       title: title_split[2..title_split.len()].join(" "),
                                       parent: p,
                                       text:  split[1..].concat()};  //everything else in the section
        b
        }
    else {
        BlockType{level_index: 255,
                  index: String::from("9999.1"),
                  title: String::from("unknown Or Not Parsed"),
                  parent: String::from("9999"),
                  text:  section}

    }
}


fn split_by_titles(text: &str) -> Vec<&str> {

    let mut sections: Vec<&str> = Vec::new();
    for section in HASHTAG_REGEX.find_iter(text) {
        sections.push(section.as_str());
    }
    sections
}

pub fn read_md(md: String) -> std::result::Result<Vec<BlockType>, String> {
    let sections: Vec<&str> = split_by_titles(&md);
    let mut blocks: Vec<BlockType> = Vec::new();
    for section in sections {
        blocks.push(parse_section(section.to_owned()))
    }
    Ok(blocks)
}


//
pub fn block1_greaterthan_block2(block1: BlockType, block2: BlockType) ->  bool {
  block1.level_index > block2.level_index 
}


//
pub fn block1_equalto_block2(block1: BlockType, block2: BlockType) ->  bool {
    block1.level_index == block2.level_index 
}


// Find the deepest level of nesting.
pub fn find_deepest_nesting(blocks: &[BlockType]) -> u8 {
    let mut deepest_block: u8 = 1;
    for block in blocks {
        if block.level_index > deepest_block {
            deepest_block = block.level_index;
        }
    }
    deepest_block
}


///Clones the blocks that are a match.
fn find_context_1(blocks: &[BlockType]) -> Result<BlockType, String> {
    if blocks.len() > 0 {
        let found_blocks = blocks.iter().filter(|block| block.index.starts_with('0')).collect::<Vec<&BlockType>>();
        if found_blocks.len() > 0 {
            Ok(found_blocks[0].clone())
        }
        else {
            Err("No context found!!".into())
        }
    }
    else {
        Err("No blocks supplied".into())
    }
}

///Clones the blocks that are a match.
fn find_block(string_level: &str, blocks: &[BlockType]) -> BlockType {
    blocks.iter().filter(|block| block.index == string_level).collect::<Vec<&BlockType>>()[0].clone()
}

///Clones the blocks that are a match.
fn _remove_block(id: String, blocks: &mut Vec<BlockType>) {
    let index = blocks.iter().position(|block| block.index == id).unwrap();
    blocks.remove(index);
}


    
//Start with an aboreal branch with an empty value and leafs with an empty vector.
//start with index 0, this will identify the context diagram, we then go by levels 1, 1.1, 1.1.1 etc
//string level is the current level, ie "0" or "1.2.3.4"
pub fn parse_md_to_aboreal_branch(branch: &mut ArborealBranchType<BlockType>, level: u8, string_level: &str, blocks: &mut Vec<BlockType>) {
    if level == 15 {return};  //hard limit on depth, just in case.
    if string_level == "0" {

        //find the context node
        let res_context = find_context_1(blocks);
        //make the context node
        match res_context {
            Ok(c) =>{branch.value = Some(c)},
            Err(_) => branch.value = None,
        }

    } 
    else {
        branch.value = Some(find_block(string_level, blocks));
    };
    let children = level_immediate_children(string_level, blocks);
    let mut local_leafs = Vec::<ArborealBranchType<BlockType>>::new();
    if !children.is_empty() {
        for child in children {
            let mut b = ArborealBranchType::<BlockType>::new();
            parse_md_to_aboreal_branch(&mut b, level + 1, &child.index,  blocks);
            local_leafs.push(b);
        }
        branch.leafs = Some(local_leafs);
    }
    else {
        branch.leafs = None;
    }
}


///Parse a markdown model into an aboreal branch
pub fn parse_markdown_aboreal(markdown_model: String) -> (ArborealBranchType<BlockType>, i32) {

    let blocks_result: std::result::Result<Vec<BlockType>, String> = read_md(markdown_model);
    let mut blocks = blocks_result.unwrap();
    let block_count = blocks.len().clone();
    let mut at = ArborealBranchType::<BlockType>::new();
    parse_md_to_aboreal_branch(&mut at, 0, "0", &mut blocks);
    (at, block_count.clone() as i32)
}

///Convert an aboreal branch of markdown to a json string
pub fn aboreal_markdown_to_json(markdown: ArborealBranchType::<BlockType>, block_count: i32) -> String {

    struct NodeState {
        parent:    bool,
        sibling:   bool,
        top_level: bool,
        context:   bool,
        distance:  i32
    }

    let mut node_state: NodeState = NodeState{parent:    false,
                                              sibling:   false,
                                              top_level: false,
                                              context:   false,
                                              distance:  0};
    //the parser state
    let mut parser_state: String = String::new();          //output being built
    let mut id_stack: Vec<String> = Vec::<String>::new();  //each id processed
    //let mut block_count: usize = 0;                        //total number of blocks to process
    let mut depth = 0;

    let stack_len = id_stack.len();

    //This closure visits every node in the markdown tree
    let string_closure2 = |block: &BlockType, has_children: bool| {

        let last_node: &String;
        let this_node: &String;
        let stack_len = id_stack.len();

        //if this is NOT the context establish node state
        if !id_stack.is_empty() {
                last_node = &id_stack[stack_len - 1];
            this_node = &block.index;
            node_state.parent = has_children;
            node_state.sibling = siblings(last_node, this_node); 
            let (parent_id, level) = level_parent(this_node);
            node_state.top_level = level == 1;
            node_state.context = parent_id == "0" && level == 0;
            node_state.distance = find_distance(this_node, last_node);
            depth += find_distance(this_node, last_node);}

        //the initial node must be a context node
        else {
            last_node = &block.index;
            node_state.parent = has_children;
            node_state.sibling = false;
            node_state.top_level = false;
            node_state.context = true;
            node_state.distance = 0;
        };

        //the json node to be emitted
        let node_string =  format!(r#"{{"id":"{}","title":"{}","description":"{}","data-in":[],"data-out":[],"children":"#, block.index, block.title, block.text);

        //Here we emit the json, a pre emitter step, the json node emitter and a post emitter step.

        //pre node emiiter
        match node_state {
            //sibling child node with the previous node
            NodeState{parent,
                      sibling:   true,
                      top_level,
                      context:   false,
                      distance} => {parser_state.push_str(",")},

            //a non sib node, if we are assending, we need to close of the deeper level            
            NodeState{parent, 
                        sibling: false,
                        top_level,
                        context:   false,
                        distance} if node_state.distance < 0 => {for _ in node_state.distance + 1 .. 0 {
                                                                    parser_state.push_str("]}")
                                                                 }
                                                                 parser_state.push_str("]},")},


            _ => ()
        };

        //node emitter
        parser_state.push_str(&node_string);

        //post node emitter
        match node_state {

            //close up a non parent node
            NodeState{parent:    false, 
                      sibling,
                      top_level,
                      context:   false,
                      distance} if node_state.distance >= 0 => parser_state.push_str("[]}"),
            
            NodeState{parent:    false, 
                      sibling,
                      top_level,
                      context:   false,
                      distance} if node_state.distance < 0 => parser_state.push_str("[]}"),

            //parent node
            NodeState{parent:    true, 
                      sibling,
                      top_level,
                      context,
                      distance} => parser_state.push_str("["),

            //A context node with no children
            NodeState{parent: false, 
                      sibling,
                      top_level,
                      context:   true,
                      distance} => parser_state.push_str("[]}"),  
            _ => ()     
        };

        //This is the gap in levels between this node and the context level
        let distance_to_context = find_distance(&block.index, "0");

        //Now tidy up if we are the last node
        if id_stack.len() as i32 >= block_count as i32 - 1 {
            let distance_to_context = find_distance(&block.index, "0");
            
            //if this is the context, dont do anything, its already closed.
            if !node_state.context {
                for _ in 0 .. distance_to_context + 1 {//+ 2 {
                    parser_state.push_str("]}");
                }
            }  
        } 
        else {
            id_stack.push(block.index.clone()); 
        }
    };

    let mut v = Vis2::new(string_closure2);
    markdown.accept(&mut v);
    parser_state
}


///Given a file location, read the markdown document from the file.
/// 
/// # Arguments
/// 
/// file - the full path of the file to read
/// 
/// # Return value
/// 
/// A result with the markdown as a string
/// 
pub fn read_model_from_file(file: &str) -> Result<String, Error> {

    let mut model = String::new();
    let mut f = File::open(file)?;
    f.read_to_string(&mut model)?;
    Ok(model)    
}


///Given a file location, read the document from the file.
/// 
/// # Arguments
/// 
/// file - the full path of the file to read
/// 
/// # Return value
/// 
/// A result with the file contents as a string
/// 
pub fn read_string_from_file(file: &str) -> Result<String, Error> {

    let mut text = String::new();
    let mut f = File::open(file)?;
    f.read_to_string(&mut text)?;
    Ok(text)    
}





#[cfg(test)]
mod simple_parser_test {
    use super::*;
    use patterns::{ArborealBranchType};
    use patterns::{Vis2, Visitable};

    static JSON_PREAMBLE: &str = "{\"procs\":";

    static CIRCULAR_MARKDOWN_1: &str = "resources/test/test_requirements_circular.md";
    static SIMPLE_MODEL_MD_1: &str = "resources/test/simple_model_1.md";

    static TEST_REQUIREMENTS_1: &str = "resources/test/test_requirements_1.md";

    static TEST_MD_1: &str = "# 0 Project SA Model";

    static TEST_MD_2: &str = "# 0 Project SA Model
    A model of project SA";

    static TEST_MD_3: &str = "# 0 Project SA Model
    A model of project SA
    
    ## 3 Kubernetes Target Architecture
    
    This subtree describes the Kubernetes organisation
    
    ### 3.1 Docker Images";



    #[test]
    fn test_find_context_1() {
        let s = read_string_from_file(TEST_REQUIREMENTS_1);
        if s.is_ok() {
            let blocks = read_md(s.unwrap());
            let c = find_context_1(&blocks.unwrap());
            assert!(c.is_ok());
            assert_eq!(c.unwrap(), BlockType{index: "0".to_string(),
                level_index: 0,
                parent: "0".to_string(),
                text: "  ".to_string(),
                title: "Requirements For The SA Modelling Tool ".to_string()});
        } else {

        }
    }

    #[test]
    fn test_split_by_titles() {
        let r1 = split_by_titles(TEST_MD_1);
        let r2 = split_by_titles(TEST_MD_2);
        let r3 = split_by_titles(TEST_MD_3);
        let mut a: Vec<&str> = Vec::new();
        a.push("# 0 Project SA Model");
        assert_eq!(a, r1);
        a.clear();
        a.push(TEST_MD_2);
        assert_eq!(a, r2);
        a.clear();

        a.push(r"# 0 Project SA Model
    A model of project SA
    
    ");

        a.push(r"## 3 Kubernetes Target Architecture
    
    This subtree describes the Kubernetes organisation
    
    ");
        a.push(r"### 3.1 Docker Images");


        assert_eq!(a, r3);

    }

    #[test]
    fn test_parse_id() {
        let id_context = "0";
        let id_1 = "1";
        let id_2 = "2";
        let id_1_1 = "1.1";
        let id_1_2_1 = "1.2.1";
        let id_1_2_5 = "1.2.5";

        assert_eq!(vec![0i32], parse_id(id_context));
        assert_eq!(vec![1i32], parse_id(id_1));
        assert_eq!(vec![2i32], parse_id(id_2));
        assert_eq!(vec![1i32,1], parse_id(id_1_1));
        assert_eq!(vec![1i32,2,1], parse_id(id_1_2_1));
        assert_eq!(vec![1i32,2,5], parse_id(id_1_2_5));

    }

    #[test]
    fn test_level_parent() -> Result<(), String> {

        let p1 = "0";
        let p2 = "1";
        let p3 = "1.1";
        let p4 = "1.1.1";

        let l1 = level_parent(p1);
        let l2 = level_parent(p2);
        let l3 = level_parent(p3);
        let l4 = level_parent(p4);
        
        assert_eq!(("0".into(),0),   l1);
        assert_eq!(("0".into(),1),   l2);
        assert_eq!(("1".into(),2),   l3);
        assert_eq!(("1.1".into(),3), l4);

        Ok(())
    }

    #[test]
    fn test_read_md() {
        let blocks_result_1: std::result::Result<Vec<BlockType>, String> = read_md(TEST_MD_1.to_string());
        let blocks_result_2: std::result::Result<Vec<BlockType>, String> = read_md(TEST_MD_2.to_string());
        let blocks_result_3: std::result::Result<Vec<BlockType>, String> = read_md(TEST_MD_3.to_string());
 
        assert!(blocks_result_1.is_ok());
        assert!(blocks_result_2.is_ok());
        assert!(blocks_result_3.is_ok());

        let result_1 = blocks_result_1.unwrap();
        let result_2 = blocks_result_2.unwrap();
        let result_3 = blocks_result_3.unwrap();

        assert_eq!(1, result_1.len());
        assert_eq!(1, result_2.len());
        assert_eq!(3, result_3.len());

        assert_eq!(0, result_1[0].level_index);
        assert_eq!("0", result_1[0].index);
        assert_eq!("Project SA Model", result_1[0].title);
        assert_eq!("", result_1[0].text);

        assert_eq!(0, result_2[0].level_index);
        assert_eq!("0", result_2[0].index);
        assert_eq!("Project SA Model", result_2[0].title);
        assert_eq!("    A model of project SA", result_2[0].text);

        assert_eq!(0, result_3[0].level_index);
        assert_eq!("0", result_3[0].index);
        assert_eq!("Project SA Model", result_3[0].title);
        assert_eq!("    A model of project SA        ", result_3[0].text);

        assert_eq!(1, result_3[1].level_index);
        assert_eq!("3", result_3[1].index);
        assert_eq!("Kubernetes Target Architecture", result_3[1].title);
        assert_eq!("        This subtree describes the Kubernetes organisation        ", result_3[1].text);

        assert_eq!(2, result_3[2].level_index);
        assert_eq!("3.1", result_3[2].index);
        assert_eq!("Docker Images", result_3[2].title);
        assert_eq!("", result_3[2].text);
    }

    #[test]
    fn test_read_md_from_file() {
        let s = read_model_from_file(CIRCULAR_MARKDOWN_1);
        match s {
            Ok(md) => {
                let blocks_result: std::result::Result<Vec<BlockType>, String> = read_md(md);
            }
            Err(e) => panic!("{}", e)
        }
        
    }
       
    //This is a developmental test for the algorithm, not an actual test.
    #[test]
    fn test_parse_md_to_aboreal_branch_to_json() -> Result<(), String> {
  
        struct NodeState {
            parent:    bool,
            sibling:   bool,
            top_level: bool,
            context:   bool,
            distance:  i32
        }

        //static MARKDOWN_MODEL: &str = "resources/test/test_requirements_1.md";
        //static JSON_MODEL:     &str = "resources/test/test_requirements_1.json"; 

        static MARKDOWN_MODEL: &str = "resources/test/level_1_1_upstep.md";
        static JSON_MODEL:     &str = "resources/test/level_1_1_upstep.json"; 

        //static MARKDOWN_MODEL: &str = "resources/test/test_model_2.md";
        //static JSON_MODEL:     &str = "resources/test/test_model_2.json";

        //static MARKDOWN_MODEL: &str = "resources/test/context_only.md";
        //static JSON_MODEL:     &str = "resources/test/context_only.json";

        //static MARKDOWN_MODEL: &str = "resources/test/level_1_1.md";
        //static JSON_MODEL:     &str = "resources/test/level_1_1.json";

        //static MARKDOWN_MODEL: &str = "resources/test/level_1_2.md";
        //static JSON_MODEL:     &str = "resources/test/level_1_2.json";

        //static MARKDOWN_MODEL: &str = "resources/test/level_1_2_1.md";
        //static JSON_MODEL:     &str = "resources/test/level_1_2_1.json";

        //static MARKDOWN_MODEL: &str = "resources/test/level_1_2_1_2.md";
        //static JSON_MODEL:     &str = "resources/test/level_1_2_1_2.json";

        let mut node_state: NodeState = NodeState{parent:    false,
                                                  sibling:   false,
                                                  top_level: false,
                                                  context:   false,
                                                  distance:  0};
        //the parser state
        let mut parser_state: String = String::new();           //output being built
        let mut id_stack: Vec<String> = Vec::<String>::new();   //each id processed
        let mut block_count: usize = 0;                         //total number of blocks to process
        let mut depth = 0;

        let mut at = ArborealBranchType::<BlockType>::new();
        let markdown = read_model_from_file(MARKDOWN_MODEL);
        let json = read_model_from_file(JSON_MODEL);  


        match markdown {
            Ok(md) => {
                let blocks_result: std::result::Result<Vec<BlockType>, String> = read_md(md);
                let mut blocks = blocks_result.unwrap();
                block_count = blocks.len().clone();
                parse_md_to_aboreal_branch(&mut at, 0, "0", &mut blocks);

                //This closure visits every node in the markdown tree
                let string_closure2 = |block: &BlockType, has_children: bool| {

                    let last_node: &String;
                    let this_node: &String;
                    let stack_len = id_stack.len();

                    //if this is NOT the context establish node state
                    if !id_stack.is_empty() {
                        last_node = &id_stack[stack_len - 1];
                        this_node = &block.index;
                        node_state.parent = has_children;
                        node_state.sibling = siblings(last_node, this_node); 
                        let (parent_id, level) = level_parent(this_node);
                        node_state.top_level = level == 1;
                        node_state.context = parent_id == "0" && level == 0;
                        node_state.distance = find_distance(this_node, last_node);
                        depth += find_distance(this_node, last_node);}

                    //the initial node must be a context node
                    else {
                        last_node = &block.index;
                        node_state.parent = has_children;
                        node_state.sibling = false;
                        node_state.top_level = false;
                        node_state.context = true;
                        node_state.distance = 0;
                    };
 
                    //the json node to be emitted
                    let node_string =  format!(r#"{{"id":"{}","title":"{}","description":"{}","data-in":[],"data-out":[],"children":"#, block.index, block.title, block.text);

                    //Here we emit the json, a pre emitter step, the json node emitter and a post emitter step.

                    //pre node emiiter
                    match node_state {
                        //sibling child node with the previous node
                        NodeState{parent,
                                  sibling:   true,
                                  top_level,
                                  context:   false,
                                  distance} => {parser_state.push_str(",")},

                        //a non sib node, if we are assending, we need to close of the deeper level            
                        NodeState{parent, 
                                  sibling: false,
                                  top_level,
                                  context:   false,
                                  distance} if node_state.distance < 0 => {for _ in node_state.distance + 1 .. 0 {
                                                                                parser_state.push_str("]}")
                                                                           }
                                                                           parser_state.push_str("]},")},


                        _ => ()
                    };

                    //node emitter
                    parser_state.push_str(&node_string);

                    //post node emitter
                    match node_state {

                        //close up a non parent node
                        NodeState{parent:    false, 
                                  sibling,
                                  top_level,
                                  context:   false,
                                  distance} if node_state.distance >= 0 => parser_state.push_str("[]}"),
                     
                        NodeState{parent:    false, 
                                  sibling,
                                  top_level,
                                  context:   false,
                                  distance} if node_state.distance < 0 => parser_state.push_str("[]}"),

                        //parent node
                        NodeState{parent:    true, 
                                  sibling,
                                  top_level,
                                  context,
                                  distance} => parser_state.push_str("["),

                        //A context node with no children
                        NodeState{parent: false, 
                                  sibling,
                                  top_level,
                                  context:   true,
                                  distance} => parser_state.push_str("[]}"),  
                        _ => ()     
                    };
                    
                    //This is the gap in levels between this node and the context level
                    let distance_to_context = find_distance(&block.index, "0");

                    //Now tidy up if we are the last node
                    if id_stack.len() >= block_count - 1 {
                        let distance_to_context = find_distance(&block.index, "0");
                        
                        //if this is the context, dont do anything, its already closed.
                        if !node_state.context {
                            for _ in 0 .. distance_to_context + 1 {//+ 2 {
                                parser_state.push_str("]}");
                            }
                        }  
                    } 
                    else {
                        id_stack.push(block.index.clone()); 
                    }
                };

                let mut v = Vis2::new(string_closure2);

                at.accept(&mut v);
                assert_eq!(json.unwrap(), parser_state);
                Ok(())
            }
            Err(e) => Err(format!("Error : {}", e))
        }
    }




    #[test]
    fn test_find_children() -> Result<(), String> {
        let s = read_model_from_file(CIRCULAR_MARKDOWN_1);
        match s {
            Ok(md) => {
                let blocks_result: std::result::Result<Vec<BlockType>, String> = read_md(md);
                let blocks = &blocks_result.unwrap();
                {
                    let found_1: Vec<BlockType> = level_immediate_children("0", blocks);
                    assert_eq!(3, found_1.len());
                }
                {
                    let found_2: Vec<BlockType> = level_immediate_children("3", blocks);
                    assert_eq!(1, found_2.len());
                }
                {
                    let found_3: Vec<BlockType> = level_immediate_children("1.1.1.1.1.1", blocks);
                    assert_eq!(1, found_3.len());
                }
                Ok(())
            }
            _     => Err(String::from("Error, could not read test data."))
        }
    }
 
    #[test]
    fn test_siblings() {
        let id_1 = "1.1.1";
        let id_2 = "1.1.2";
        assert!(siblings(id_1, id_2));
        let id_1 = "1.1.1";
        let id_2 = "1.1";
        assert!(!siblings(id_1, id_2));
        let id_1 = "1.1";
        let id_2 = "1.1.1";
        assert!(!siblings(id_1, id_2));
        let id_1 = "0";
        let id_2 = "0";
        assert!(!siblings(id_1, id_2)); //there are never two contexts in a model
        let id_1 = "1";
        let id_2 = "1.1.1";
        assert!(!siblings(id_1, id_2));
        let id_1 = "1.2.1";
        let id_2 = "1.3.1";
        assert!(!siblings(id_1, id_2));
    }
 
    #[test]
    fn test_find_distance() {
        let id_1 = "1.1.1";
        let id_2 = "1.1.2";
        assert_eq!(0, find_distance(id_1, id_2));
        let id_1 = "1.1.1";
        let id_2 = "1.1";
        assert_eq!(1, find_distance(id_1, id_2));
        let id_1 = "0";
        let id_2 = "0";
        assert_eq!(0, find_distance(id_1, id_2));
        let id_1 = "1.1.1";
        let id_2 = "1";
        assert_eq!(2, find_distance(id_1, id_2));
        let id_1 = "1.3.1.1.1.1";
        let id_2 = "1.2.1";
        assert_eq!(3, find_distance(id_1, id_2));
        let id_1 = "1";
        let id_2 = "0";
        assert_eq!(0, find_distance(id_1, id_2));
        let id_1 = "1.1";
        let id_2 = "0";
        assert_eq!(1, find_distance(id_1, id_2));
        let id_1 = "1.1.1";
        let id_2 = "0";
        assert_eq!(2, find_distance(id_1, id_2));
        let id_1 = "8.8";
        let id_2 = "0";
        assert_eq!(1, find_distance(id_1, id_2));
        let id_1 = "1.3.6";
        let id_2 = "1.3.5.5.4";
        assert_eq!(-2, find_distance(id_1, id_2));
    }
    
    //read the markdown document into the tree
    //read the tree out to json
    #[test]
    fn test_markdown_parser_1() -> Result<(), String>{
        let markdown_model = read_model_from_file(SIMPLE_MODEL_MD_1);
        match markdown_model {
            Ok(md) => {let (markdown, block_count ) = parse_markdown_aboreal(md);
                               let json = aboreal_markdown_to_json(markdown, block_count);
                               assert!(json.ends_with("]}]}"));
                               Ok(())
                              },
            _ => Err(String::from("Error, could not read test data."))
        }
    }

    #[test]
    fn test_markdown_parser_2() -> Result<(), String>{
        let markdown_model = read_model_from_file(CIRCULAR_MARKDOWN_1);
        match markdown_model {
            Ok(md) => {let (markdown, block_count ) = parse_markdown_aboreal(md);
                               let json = aboreal_markdown_to_json(markdown, block_count);
                               assert!(json.ends_with("}]}]}]}]}]}]}]}"));
                               Ok(())
                              },
            _ => Err(String::from("Error, could not read test data."))
        }
    }
   
    //general test framework
    fn test_framework(markdown_file: &str, json_file: &str) -> core::result::Result<(), String> {

        //parse markdown
        let markdown_model = read_model_from_file(markdown_file);
        assert_eq!(true, markdown_model.is_ok());
        let (markdown, block_count ) = parse_markdown_aboreal(markdown_model.unwrap());
        let json = aboreal_markdown_to_json(markdown, block_count);

        //check the result against the markdown
        let e = super::read_string_from_file(json_file);
        let expected = e.unwrap();

        //we have to add the procs wrapper at this point
        let mut s: String = String::new();
  
        s.push_str(JSON_PREAMBLE); //The preamble
        s.push_str(&json);         //the result
        s.push_str("}");           //the closing bracket

        assert_eq!(expected, s);
        Ok(())
    }


    #[test]
    fn test_markdown_parser_context_only() -> core::result::Result<(), String> {

        //test data and expected result
        static CONTEXT_ONLY_MD:   &str = "resources/test/context_only.md";
        static CONTEXT_ONLY_JSON: &str = "resources/test/context_only.json";

        test_framework(CONTEXT_ONLY_MD, CONTEXT_ONLY_JSON) 
    }
 
    #[test]
    fn test_markdown_parser_level_1_1() -> core::result::Result<(), String> {

        //test data and expected result
        static LEVEL_1_1_MD:   &str = "resources/test/level_1_1.md";
        static LEVEL_1_1_JSON: &str = "resources/test/level_1_1.json";

        test_framework(LEVEL_1_1_MD, LEVEL_1_1_JSON) 
    }

    #[test]
    fn test_markdown_parser_level_1_2() -> core::result::Result<(), String> {

        //test data and expected result
        static LEVEL_1_2_MD:   &str = "resources/test/level_1_2.md";
        static LEVEL_1_2_JSON: &str = "resources/test/level_1_2.json";

        test_framework(LEVEL_1_2_MD, LEVEL_1_2_JSON) 
    }
 
  
    #[test]
    fn test_markdown_parser_level_1_2_1() -> core::result::Result<(), String> {

        //test data and expected result
        static LEVEL_1_2_1_MD:   &str = "resources/test/level_1_2_1.md";
        static LEVEL_1_2_1_JSON: &str = "resources/test/level_1_2_1.json";

        test_framework(LEVEL_1_2_1_MD, LEVEL_1_2_1_JSON)
    }
 
    #[test]
    fn test_markdown_parser_level_1_2_1_2() -> core::result::Result<(), String> {

        //test data and expected result
        static LEVEL_1_2_1_2_MD:   &str = "resources/test/level_1_2_1_2.md";
        static LEVEL_1_2_1_2_JSON: &str = "resources/test/level_1_2_1_2.json";

        test_framework(LEVEL_1_2_1_2_MD, LEVEL_1_2_1_2_JSON)
    }

    #[test]
    fn test_markdown_parser_test_requirements_1() -> core::result::Result<(), String> {
        //This is a complete model from the top so no procs prelude required in the json
        //test data and expected result
        static LEVEL_1_2_1_2_MD:   &str = "resources/test/test_requirements_1.md";
        static LEVEL_1_2_1_2_JSON: &str = "resources/test/test_requirements_1.json";

        //parse markdown
        let markdown_model = read_model_from_file(LEVEL_1_2_1_2_MD);
        assert_eq!(true, markdown_model.is_ok());
        let (markdown, block_count ) = parse_markdown_aboreal(markdown_model.unwrap());
        let json = aboreal_markdown_to_json(markdown, block_count);

        //check the result against the markdown
        let e = super::read_string_from_file(LEVEL_1_2_1_2_JSON);
        let expected = e.unwrap();

        //we have to add the procs wrapper at this point
        let mut s: String = String::new();

        s.push_str(&json);         //the result

        assert_eq!(expected, s);
        Ok(())

    }

}