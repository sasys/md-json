//! # A Parser For The Json Version Of The Model
 
/// Given a json model, extracts the textual hierarchy of ids, titles and body texts 
/// into a composite structure.
/// 
/// # Input A serde json document representing the complete model
/// 
/// # Output A composite instances containing the ids, titles and body descriptions.
/// 


use std::fs::File;
use std::io::prelude::*;
use http::{HeaderMap, HeaderValue, header::ACCEPT};
use patterns::{ArborealBranchType};
use std::io::{Read, Error};



static PROC_ID: &str = "id";
static TITLE: &str = "title";
static DESCRIPTION: &str = "description";
static CHILDREN: &str = "children";

pub fn make_string_from_serde_value(index: u16, model: &serde_json::Value<>) -> String {
    let mut text: String = String::from("");
    for _i in 0..index {
        text.push_str("#");
    }
    text.push_str(" ");
    text.push_str(&model[PROC_ID].as_str().unwrap_or("9999"));
    text.push_str(" ");
    text.push_str(&model[TITLE].as_str().unwrap_or("No Title"));
    text.push_str("\n");
    text.push_str(&model[DESCRIPTION].as_str().unwrap_or("No description."));
    //text.push_str("\n");
    text
}


//Start with an aboreal branch with an empty value and leafs with an empty vector.
//start with index 1.
pub fn parse_serde_to_aboreal_branch(branch: &mut ArborealBranchType<String>, index: u16, model: &serde_json::Value<>) {
    let s = make_string_from_serde_value(index, model);
    branch.value = Some(s);


    if model[CHILDREN].is_array() && !model[CHILDREN].as_array().unwrap().is_empty() {
        let children = model[CHILDREN].as_array();
        match children {
            Some(_) => {    
                            let mut local_leafs = Vec::<ArborealBranchType<String>>::new();
                            for child in children.unwrap() {
                                let mut b1 = ArborealBranchType::<String>{value: None, 
                                                                         leafs: Some(Vec::<ArborealBranchType<String>>::new())};
                                parse_serde_to_aboreal_branch(&mut b1, index + 1, child);
                                local_leafs.push(b1);
                            }
                            branch.leafs = Some(local_leafs);
                        }
            None    => {} 
        }
    } else {
        {}
    }
}



///Parse a model into a composite
pub fn parse_serde_aboreal(model: &serde_json::Value<>) -> ArborealBranchType<String> {
    let mut b = ArborealBranchType{value: None, 
                                  leafs: Some(Vec::<ArborealBranchType<String>>::new())};
    parse_serde_to_aboreal_branch(&mut b, 1, model);
    b
}

///Given a url, GET the model from the url and create a text based serde_json value
/// 
/// # Arguments
/// 
/// url - the url of a model in the model service api
/// 
/// # Return value
/// 
/// A result with a serde json model
/// 
// pub fn read_model_from_url(url: &str) -> Result<serde_json::Result<serde_json::Value<>>, String> {
//
//     let mut headers: http::HeaderMap = HeaderMap::new();
//     headers.insert(ACCEPT, HeaderValue::from_str("application/json").unwrap());
//
//
//
//     async {
//         // In the case of web assembly target, remember that the browser doesn't do synch io so the get
//         // in here needs to be async.
//         let mut client = request::Client::new().request(reqwest::Method::get, url).headers(headers);
//         let model_r = client.send().await;
//
//         match model_r {
//             Ok(model) => {
//                 match model {
//                     Ok(r) => match r.status() {
//                         reqwest::StatusCode::OK => Ok(serde_json::from_str(&r.text().unwrap())),
//                         _ => Err(r.status().to_string())
//                     },
//                     Err(e) => Err(format!("Failed to read uris : {:?}", e)),
//                 },
//                 Err(e) => Err(format!("Failed to read uris : {:?}", e))
//             }
//             Err(e) => Err(format!("Failed to read uris : {:?}", e))
//         }
//     }
// }

///Given a file location, read the model from the file and create a text based serde_json value
/// 
/// # Arguments
/// 
/// file - the full path of the file to read
/// 
/// # Return value
/// 
/// A result with a serde json model
/// 
pub fn read_model_from_file(file: &str) -> Result<serde_json::Result<serde_json::Value<>>, String> {

    let mut model = String::new();
    match File::open(file) {
        Ok(mut r)  => {match r.read_to_string(&mut model) {
                        Ok(_)  => Ok(serde_json::from_str(&model)),
                        Err(e) => Err(e.to_string())}
        },
        Err(e) =>  Err(e.to_string())
    }
}

///Given a file location, read the document from the file.
/// 
/// # Arguments
/// 
/// file - the full path of the file to read
/// 
/// # Return value
/// 
/// A result with the file contents as a string
/// 
pub fn read_string_from_file(file: &str) -> Result<String, Error> {

    let mut text = String::new();
    let mut f = File::open(file)?;
    f.read_to_string(&mut text)?;
    Ok(text)    
}

#[cfg(test)]
mod simple_parser_test {
    use super::{read_model_from_file};
    use patterns::{Vis, ArborealBranchType};
    use patterns::Visitable;
    use std::fs::File;
    use std::io::Read;
    extern crate serde;
    extern crate serde_json;
    use serde_json::Result;

    static CHILDREN: &str = "children";
    static PROCS: &str = "procs";
    static TEST_MODEL_2: &str = "resources/test/test_model_2.json";


    ///Find the type of a data structure
    fn type_of<T>(_: &T) -> &str {
        std::any::type_name::<T>()
    }


    #[test]
    fn test_simple_model_parser() {

        let test_model_1 = File::open(TEST_MODEL_2);
        assert_eq!(true, test_model_1.is_ok());
        let mut test_model = String::new();
        let r = test_model_1.unwrap().read_to_string(&mut test_model);
        assert_eq!(r.is_ok(), true);
        let result: Result<serde_json::Value<>> = serde_json::from_str(&test_model);
        assert_eq!(true, result.is_ok());
        let r = result.unwrap();
        assert_eq!("2", r[PROCS][CHILDREN][0]["id"]);
        assert_eq!("Meta data Production", r[PROCS][CHILDREN][0]["title"]);
        assert_eq!("", r[PROCS][CHILDREN][0]["description"]);
    }

    #[test]
    fn test_read_model_from_file_2() {

        let r = read_model_from_file(TEST_MODEL_2);
        assert_eq!(true, r.is_ok());

        let r1 = r.unwrap();
        assert_eq!(true, r1.is_ok());

        let test_model_2 = r1.unwrap();
        assert_eq!("serde_json::value::Value", type_of(&test_model_2));
        assert_eq!("serde_json::value::Value", type_of(&test_model_2));
        assert_eq!("2", test_model_2[PROCS][CHILDREN][0]["id"]);
        assert_eq!("Meta data Production", test_model_2[PROCS][CHILDREN][0]["title"]);
        assert_eq!("", test_model_2[PROCS][CHILDREN][0]["description"]);
        assert_eq!("Proc 1 description.", test_model_2[PROCS][CHILDREN][1]["description"]);
    }


    // #[test]
    // fn test_parse_serde_to_aboreal_branch_context_only() {

    //     let mut parser_state: String = String::new();

    //     let string_closure = |text: &String| {
    //         parser_state.push_str(text);
    //         parser_state.push_str("\n");
    //     };

    //     let r = read_model_from_file(TEST_MODEL_1);
    //     assert_eq!(true, r.is_ok());
    //     let r1 = r.unwrap();
    //     assert_eq!(true, r1.is_ok());
    //     let test_model_1: serde_json::Value = r1.unwrap();    
    //     let test_model_1_procs = &test_model_1[PROCS]; 
    //     let doc: ArborealBranchType<String> =  super::parse_serde_aboreal(&test_model_1_procs);
    //     let mut v = Vis::new(string_closure);
    //     doc.accept(&mut v);
    //     println!("\n\nThe string state : \n: {}\n\n", parser_state);
    // }

    #[test]
    fn test_parse_serde_to_aboreal_branch_full_model() {

        let mut parser_state: String = String::new();

        let string_closure = |text: &String| {
            parser_state.push_str(text);
            parser_state.push_str("\n");
        };

        let r = read_model_from_file(TEST_MODEL_2);
        assert_eq!(true, r.is_ok());
        let r1 = r.unwrap();
        assert_eq!(true, r1.is_ok());
        let test_model_1: serde_json::Value = r1.unwrap();    
        let test_model_1_procs = &test_model_1[PROCS]; 
        let doc: ArborealBranchType<String> =  super::parse_serde_aboreal(&test_model_1_procs);
        let mut v = Vis::new(string_closure);
        doc.accept(&mut v);
        println!("\n\nThe string state : \n: {}\n\n", parser_state);
    }

    //general test framework
    fn test_framework(json_file: &str, markdown_file: &str) -> core::result::Result<(), String> {
        let mut parser_state: String = String::new();

        let string_closure = |text: &String| {
            parser_state.push_str(text);
            parser_state.push_str("\n");
        };
 
        let r = read_model_from_file(json_file);
        assert_eq!(true, r.is_ok());
        let r1 = r.unwrap();
        assert_eq!(true, r1.is_ok());
        let test_model_1: serde_json::Value = r1.unwrap();    
        let test_model_1_procs = &test_model_1[PROCS]; 
        let doc: ArborealBranchType<String> =  super::parse_serde_aboreal(&test_model_1_procs);
        let mut v = Vis::new(string_closure);
        doc.accept(&mut v);

        //check the result against the markdown
        let e = super::read_string_from_file(markdown_file);
        let expected = e.unwrap();
        assert_eq!(expected, parser_state);
        Ok(()) 
    }

    #[test]
    fn test_parse_serde_to_aboreal_branch_context_only() -> core::result::Result<(), String> {

        //test data and expected result
        static CONTEXT_ONLY_JSON: &str = "resources/test/context_only.json";
        static CONTEXT_ONLY_MD:   &str = "resources/test/context_only.md";

        test_framework(CONTEXT_ONLY_JSON, CONTEXT_ONLY_MD)
    }
 
    #[test]
    fn test_parse_serde_to_aboreal_branch_level_1_1() -> core::result::Result<(), String> {

        //test data and expected result
        static LEVEL_1_1_JSON: &str = "resources/test/level_1_1.json";
        static LEVEL_1_1_MD:   &str = "resources/test/level_1_1.md";

        test_framework(LEVEL_1_1_JSON, LEVEL_1_1_MD) 
    }

    #[test]
    fn test_parse_serde_to_aboreal_branch_level_1_2() -> core::result::Result<(), String> {

        //test data and expected result
        static LEVEL_1_2_JSON: &str = "resources/test/level_1_2.json";
        static LEVEL_1_2_MD:   &str = "resources/test/level_1_2.md";

        test_framework(LEVEL_1_2_JSON, LEVEL_1_2_MD) 
    }
 
    #[test]
    fn test_parse_serde_to_aboreal_branch_level_1_2_1() -> core::result::Result<(), String> {

        //test data and expected result
        static LEVEL_1_2_1_JSON: &str = "resources/test/level_1_2_1.json";
        static LEVEL_1_2_1_MD:   &str = "resources/test/level_1_2_1.md";

        test_framework(LEVEL_1_2_1_JSON, LEVEL_1_2_1_MD) 
    }
 
    #[test]
    fn test_parse_serde_to_aboreal_branch_level_1_2_1_2() -> core::result::Result<(), String> {

        //test data and expected result
        static LEVEL_1_2_1_2_JSON: &str = "resources/test/level_1_2_1_2.json";
        static LEVEL_1_2_1_2_MD:   &str = "resources/test/level_1_2_1_2.md";

        test_framework(LEVEL_1_2_1_2_JSON, LEVEL_1_2_1_2_MD) 
    }
}
