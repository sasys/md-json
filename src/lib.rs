//Copyright SoftwareByNumbers Ltd 2021

//! # A Parser For The Json Version Of The Model
 
/// Given a json model, extracts the textual hierarchy of ids, titles and body texts 
/// into a composite structure and returns the stringified version as markdown.
/// 
/// # Input A serde json document representing the complete model
/// 
/// # Output A composite instances containing the ids, titles and body descriptions.
/// 


pub mod parsers;

use patterns::{ArborealBranchType, Vis};
use patterns::Visitable;
use crate::parsers::md_parser::{parse_serde_aboreal};
use crate::parsers::md_json_parser::{parse_markdown_aboreal, aboreal_markdown_to_json};


/// Given a serde json value of a model, create a composite
/// Call this with the child of the model proces, model[PROCS]
/// 
/// Note, this is recursive!! - take care.
/// 
/// # Arguments
/// model - a serde_json::Value representing a model
/// 
/// # Return 
/// A String representing the markdown version of the model
///
pub fn parse_serde(model: &serde_json::Value<>) -> String {

    let mut parser_state: String = String::new();

    let string_closure = |text: &String| {
        parser_state.push_str(text);
        parser_state.push_str("\n");
    };
    let mut visitor = Vis::new(string_closure);
    let doc: ArborealBranchType<String> =  parse_serde_aboreal(model);
    doc.accept(&mut visitor);
    parser_state
}


pub fn parse_json(json_string: String) -> String {

    let json_model: serde_json::Result<serde_json::Value> =
        serde_json::from_str(&json_string);
    match json_model {
        Ok(j)  => {parse_serde(&j)},
        Err(e) => {println!("\n\nThere was an error: {}\n\n", e);
                   String::from(format!("# Error\n There was an error: {}", e))}
    }
}



/// Given a markdown string that has been derived from the parse serde 
/// method above convert that string into a json string.
/// 
/// Note, this is recursive!! - take care.
/// 
/// # Arguments
/// model - a markdown string
/// 
/// # Return 
/// A JSON string
///
pub fn parse_markdown(markdown_string: String) -> String {
    let (tree, count) = parse_markdown_aboreal(markdown_string);
    aboreal_markdown_to_json(tree, count)
}


#[cfg(test)]
mod simple_parse_markdown_test {
    use crate::parsers::md_parser;
    use crate::parsers::md_json_parser;
    use std::io::{Read, Error};
    use std::fs::File;

    //When we convert to json we need to add this to make a model component
    static JSON_PREAMBLE: &str = "{\"procs\":";

    //Used to find the procs section of the json model.
    static PROCS: &str = "procs";

    //Just read a text file
    fn read_text_file(file: &str) -> Result<String, Error> {
        let mut file_text = String::new();
        let mut f = File::open(file)?;
        f.read_to_string(&mut file_text)?;
        Ok(file_text)
    }
  
    #[test]
    fn test_full_model_from_sa_tool() {

        static TEST_MODEL_2_JSON: &str = "resources/test/test_model_2.json";
        static TEST_MODEL_2_MD:   &str = "resources/test/test_model_2.md";

        //test we get the expected markdown from json
        let r = md_parser::read_model_from_file(TEST_MODEL_2_JSON);
        assert!(r.is_ok());
        let expected_md_res = read_text_file(TEST_MODEL_2_MD);
        assert_eq!(true, expected_md_res.is_ok());
        let r1 = r.unwrap();
        assert_eq!(true, r1.is_ok());
        let test_model_2: serde_json::Value = r1.unwrap();    
        let test_model_2_procs = &test_model_2[PROCS]; 
        let res: String = super::parse_serde(&test_model_2_procs);
        assert_eq!(expected_md_res.unwrap(), res);
    }


    #[test]
    fn test_full_model_from_string() {

        static TEST_MODEL_2_JSON: &str = "resources/test/test_model_2.json";
        static TEST_MODEL_2_MD:   &str = "resources/test/test_model_2.md";

        //test we get the expected markdown from json
        let r = md_parser::read_model_from_file(TEST_MODEL_2_JSON);
        assert!(r.is_ok());
        let expected_md_res = read_text_file(TEST_MODEL_2_MD);
        assert_eq!(true, expected_md_res.is_ok());
        let r1 = r.unwrap();
        assert_eq!(true, r1.is_ok());
        let test_model_2: serde_json::Value = r1.unwrap();    
        let test_model_2_procs = &test_model_2[PROCS]; 
        //Now write out the model as a String because we are testing the string interface
        let string_test_model_2_procs: String = String::from(&serde_json::to_string(test_model_2_procs).unwrap());
        let res: String = super::parse_json(string_test_model_2_procs);
        assert_eq!(expected_md_res.unwrap(), res);
    }


    #[test]
    fn test_parse_markdown_2() -> Result<(), String> {

        static REQS_MODEL_1_MD: &str = "resources/test/test_requirements_1.md";
        static REQS_MODEL_1_JSON: &str = "resources/test/test_requirements_1.json";
 
        //The expected json output
        let e = read_text_file(REQS_MODEL_1_JSON);
        let expected = e.unwrap();

        let markdown_string = md_json_parser::read_model_from_file(REQS_MODEL_1_MD);
        match markdown_string {
            Ok(md) => {let json = super::parse_markdown(md);
                               assert_eq!(expected, json);
                               Ok(())}
            _ => {Err(String::from("Error, could not read test data."))}   
        } 
    }

    //General test method for round trip tests
    fn test_round_trip(json_filename: &str) -> core::result::Result<(), String> {
        //process the json model
        let model_json_result = md_parser::read_model_from_file(json_filename);
        assert_eq!(true, model_json_result.is_ok());
        let model_json_result_r = model_json_result.unwrap();
        assert_eq!(true, model_json_result_r.is_ok());
        let model_json: serde_json::Value = model_json_result_r.unwrap();

        let model_json_procs = &model_json[PROCS];

        //convert json to markdown
        let model_markdown_string: String = super::parse_serde(&model_json_procs);

        //convert the markdown back to json
        let round_tripped_json = super::parse_markdown(model_markdown_string);

        //we have to add the procs wrapper at this point
        let mut s: String = String::new();

        s.push_str(JSON_PREAMBLE);       //The preamble
        s.push_str(&round_tripped_json); //the result
        s.push_str("}");                 //the closing bracket

        //check the round tripped json against the original json
        let e = read_text_file(json_filename);
        let original_json = e.unwrap();

        assert_eq!(original_json, s);
        Ok(())
    }

    #[test]
    fn test_round_trip_context_only() -> core::result::Result<(), String> {

        //test data and expected result
        static CONTEXT_ONLY_JSON: &str = "resources/test/context_only.json";
        test_round_trip(CONTEXT_ONLY_JSON)
    }

    #[test]
    fn test_round_trip_level_1_1() -> core::result::Result<(), String> {

        //test data and expected result
        static LEVEL_1_1_JSON: &str = "resources/test/level_1_1.json";
        test_round_trip(LEVEL_1_1_JSON)
    }

    #[test]
    fn test_round_trip_level_1_2() -> core::result::Result<(), String> {

        //test data and expected result
        static LEVEL_1_2_JSON: &str = "resources/test/level_1_2.json";
        test_round_trip(LEVEL_1_2_JSON)
    }

    #[test]
    fn test_round_trip_level_1_2_1() -> core::result::Result<(), String> {

        //test data and expected result
        static LEVEL_1_2_1_JSON: &str = "resources/test/level_1_2_1.json";
        test_round_trip(LEVEL_1_2_1_JSON)
    }

    #[test]
    fn test_round_trip_level_1_2_1_2() -> core::result::Result<(), String> {

        //test data and expected result
        static LEVEL_1_2_1_2_JSON: &str = "resources/test/level_1_2_1_2.json";
        test_round_trip(LEVEL_1_2_1_2_JSON)
    }

    //Fow a complete model, we dont need to add the procs conatiner to the json!!
    #[test]
    fn test_round_trip_full_model_1() -> core::result::Result<(), String> {

        //test data and expected result
        static TEST_MODEL_JSON: &str = "resources/test/test_requirements_1.json";

        //process the json model
        let model_json_result = md_parser::read_model_from_file(TEST_MODEL_JSON);
        assert_eq!(true, model_json_result.is_ok());
        let model_json_result_r = model_json_result.unwrap();
        assert_eq!(true, model_json_result_r.is_ok());
        let model_json: serde_json::Value = model_json_result_r.unwrap();

        //convert json to markdown
        let model_markdown_string: String = super::parse_serde(&model_json);

        //convert the markdown back to json
        let round_tripped_json = super::parse_markdown(model_markdown_string);

        //check the round tripped json against the original json
        let e = read_text_file(TEST_MODEL_JSON);
        let original_json = e.unwrap();

        assert_eq!(original_json, round_tripped_json);
        Ok(())



    }
}