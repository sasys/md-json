//! Examples of the use of the json and markdown parsers.
/// In this example we use a file of json which has been derived from an actual model. 


extern crate sa_md;

use serde_json;
use std::io::{Read, Error};
use std::fs::File;

use sa_md::parse_serde;
use sa_md::parse_markdown;


//A json test model.
static TEST_MODEL_2: &str = "resources/test/test_model_2.json";

//Used to find the procs section of the json model.
static PROCS: &str = "procs";

//just a text file reader
fn read_text_file(file: &str) -> Result<String, Error> {
    let mut file_text = String::new();
    let mut f = File::open(file)?;
    f.read_to_string(&mut file_text)?;
    Ok(file_text)
}

//A file reader that creates serde json values
pub fn read_model_from_file(file: &str) -> Result<serde_json::Result<serde_json::Value<>>, Error> {
    let text_file: String = read_text_file(file)?;
    Ok(serde_json::from_str(&text_file))
}

// Read in a json test model 
// Convert to markdown
// Convert back to json
fn main() {

    let r = read_model_from_file(TEST_MODEL_2);
    let r1 = r.unwrap();
    let test_model_1: serde_json::Value = r1.unwrap();    
    let test_model_1_procs = &test_model_1[PROCS]; 

    let markdown = parse_serde(&test_model_1_procs);

    println!("\nJson Model To Markdown");
    println!("\n============");
    println!("{}\n", markdown);
    println!("\n============\n");

    let json: String = parse_markdown(markdown);
    let serde_json: serde_json::Result<serde_json::Value> = serde_json::from_str(&json);

    println!("\nMarkdown Model Back To Json");
    println!("\n============");
    match serde_json {
        Ok(j) => println!("{}", serde_json::to_string_pretty(&j).unwrap()),
        Err(e) => println!("{}", e),
        
    };
    println!("\n============\n");

}