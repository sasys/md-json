# Convert To/From Json Markdown Model Representations 

[![CircleCI](https://circleci.com/bb/sasys/md-json.svg?style=svg)](https://circleci.com/bb/sasys/md-json)

Provides a library that supports the conversion of model json to model markdown, and model markdown to model json.
Currently only processes the id, title and description so the flows (in and out) are lost.  The transformed or edited titles and descriptions can be merged back into the original model until we have a markdown based editor for the flows.


## Build And Test

``` rust
cargo clean
cargo build
cargo test
```

## Examples

```rust
cargo run --example example
```


## As WASM
wasm-pack build --target web


